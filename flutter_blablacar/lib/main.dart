import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'localization/locale_provider.dart';
import 'localization/localization.dart';
import 'ui/pages/home/home_page.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
//      systemNavigationBarColor: Colors.white,
      statusBarBrightness: Brightness.dark,
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark));

  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
  runApp(MultiProvider(providers: await initProviders(), child: MyApp()));
}

initProviders() async {
  var locale = LocaleProvider(await _fetchLocale());
  return [
    ChangeNotifierProvider.value(value: locale),
    ChangeNotifierProvider(create: (_) => InAsyncCallProvider()),

  ];
}

class InAsyncCallProvider extends ChangeNotifier{
}

Future<Locale> _fetchLocale() async {
  var prefs = await SharedPreferences.getInstance();
  if (prefs.getString('language_code') == null) {
    return null;
  }
  return Locale(
      prefs.getString('language_code'), prefs.getString('country_code'));
}

class MyApp extends StatelessWidget {
  int _localeCallbackCounter = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      locale: Provider.of<LocaleProvider>(context).value,
      localeResolutionCallback: (deviceLocale, supportedLocales) {
        Locale locale;
        if (_localeCallbackCounter % 2 == 0) {
          print('1');
          if (Provider.of<LocaleProvider>(context).value != null) {
            print('2');
            if (Provider.of<LocaleProvider>(context).value.toString() !=
                deviceLocale.toString()) {
              print('3');
              Provider.of<LocaleProvider>(context).valueWithoutNotify =
                  supportedLocales.contains(deviceLocale)
                      ? deviceLocale
                      : Locale('ru', 'RU');
            }
            print('4');
            locale = Provider.of<LocaleProvider>(context).value;
          } else {
            print('5');
            if (supportedLocales.contains(deviceLocale)) {
              print('6');
              Provider.of<LocaleProvider>(context).valueWithoutNotify =
                  deviceLocale;
            } else {
              print('7');
              Provider.of<LocaleProvider>(context).valueWithoutNotify =
                  const Locale('ru', 'RU');
            }
            locale = Provider.of<LocaleProvider>(context).value;
          }
        } else {
          print('8!!!!');
          if (Provider.of<LocaleProvider>(context).value != null) {
            print('9!!!!');
            locale = Provider.of<LocaleProvider>(context).value;
          } else {
            print('10!!!!');
            locale = const Locale('ru', 'RU');
          }
        }
        print('counter: $_localeCallbackCounter');

//          Intl.defaultLocale = 'en';
        Intl.defaultLocale = locale.languageCode;
        _localeCallbackCounter++;

        return locale;
      },
      localizationsDelegates: [
        const MyLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('ru', 'RU'),
      ],
      home: HomePage(),
    );
  }
}
