import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blablacar/localization/languages/word_keys.dart';
import 'package:flutter_blablacar/localization/localization.dart';
import 'file:///D:/Flutter/my-flutter-project/flutter_blablacar/lib/ui/pages/profile_page/profile_page.dart';
import 'package:flutter_blablacar/ui/pages/travel_history/travel_history.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _buildBottomBar(),
      body: SafeArea(child: _buildBody()),
    );
  }

  Widget _buildBody() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _buildAppBar(),
        Expanded(
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Image.asset("assets/images/home_image.jpg",
                  fit: BoxFit.fitHeight),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      "У вас пока нет будущих поездок!",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 30,
                      ),
                      textAlign: TextAlign.center,
                    )),
              )
            ],
          ),
        ),
        Padding(
            padding: const EdgeInsets.only(left: 20),
            child: GestureDetector(
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => TravelHistory())),
              child: Container(
                color: Colors.transparent,
                height: 60,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('История поездок'),
                    Padding(
                      padding: const EdgeInsets.only(right: 25),
                      child: Icon(Icons.arrow_forward_ios),
                    )
                  ],
                ),
              ),
            ))
      ],
    );
  }

  Widget _buildBottomBar() {
    return Container(
      height: 60,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.search),
              Text(AppLocalizations.of(context, WordKeys.search))
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.home),
              Text(AppLocalizations.of(context, WordKeys.home))
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.add_circle_outline),
              Text(AppLocalizations.of(context, WordKeys.offer))
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildAppBar() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            GestureDetector(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) {
                    return ProfilePage();
                  },
                  fullscreenDialog: true)),
              child: ClipOval(
                  child: Image.asset(
                "assets/images/profile_avatar.jpg",
                fit: BoxFit.fill,
                width: 40,
                height: 40,
              )),
            ),
            Icon(Icons.chat)
          ],
        ),
      ),
    );
  }
}
