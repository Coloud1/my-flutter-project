import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blablacar/localization/languages/word_keys.dart';
import 'package:flutter_blablacar/localization/localization.dart';
import 'package:flutter_blablacar/ui/utils/lib.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  int _index = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        onCompletion: (value) {
          setState(() {
            _index = value;
          });
        },
      ),
      body: SafeArea(
        child: SingleChildScrollView(child: _buildBody()),
      ),
    );
  }

  Widget _buildBody() {
    return IndexedStack(
      index: _index,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _buildAvatar(),
          ],
        ),
        Text('Настройки')
      ],
    );
  }

  Widget _buildAvatar() {
    return Padding(
        padding: const EdgeInsets.only(left: 50, top: 30),
        child: Row(
          children: [
           Stack(
             children: [
               ClipOval(
                   child: Image.asset(
                     "assets/images/profile_avatar.jpg",
//              key: Key(""),
                     width: 50,
                     height: 50,
                     fit: BoxFit.fill,
                   )),
               Align(
                   alignment: Alignment.bottomRight,
                   child: Icon(Icons.record_voice_over))
             ],
           ),
            Padding(
              padding: const EdgeInsets.only(left: 15, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Ivan'),
                  Text(
                      "${AppLocalizations.of(context, WordKeys.experience)}: амбассадор")
                ],
              ),
            )
          ],
        ));
  }
}
