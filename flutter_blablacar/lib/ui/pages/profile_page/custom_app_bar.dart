import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blablacar/localization/languages/word_keys.dart';
import 'package:flutter_blablacar/localization/localization.dart';

typedef Value = void Function(int value);

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  final Value onCompletion;

  const CustomAppBar({Key key, this.onCompletion}) : super(key: key);

  @override
  _CustomAppBarState createState() => _CustomAppBarState();

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(95);
}

class _CustomAppBarState extends State<CustomAppBar> {
  int a = 0;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 65),
                    child: Container(),
                  ),
                  Text('Профиль'),
                  Padding(
                    padding: const EdgeInsets.only(right: 15),
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: Text(
                        'Закрыть',
                        style: TextStyle(color: Colors.blue),
                      ),
                    ),
                  )
                ],
              ),
            ),
            CupertinoSegmentedControl(
              selectedColor: Colors.blue,
              pressedColor: Colors.blue.withOpacity(0.1),
              groupValue: a,
              onValueChanged: (int value) {
                setState(() {
                  a = value;
                });
                widget.onCompletion(value);
              },
              children: {
                0: Text("${AppLocalizations.of(context, WordKeys.about)}"),
                1: Text("${AppLocalizations.of(context, WordKeys.setting)}")
              },
            ),
            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: const Divider(
                height: 0.6,
                thickness: 0.5,
                color: Colors.grey,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
