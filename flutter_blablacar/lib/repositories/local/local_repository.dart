import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class LocalRepository {
  static const _TOKEN_KEY = "TOKEN_KEY";
  static const _LOCALE_KEY = "LOCALE_KEY";
  static const _COLD_START = "COLD_START";
  static const _USER_KEY = 'USER_KEY';
  static const _PASSWORD_KEY = 'PASSWORD_KEY';
  static const _USER_ID = 'USER_ID';
  SharedPreferences prefs;

  LocalRepository();

  static LocalRepository _instance;

  static LocalRepository get instance {
    if (_instance == null) {
      _instance = LocalRepository();
    }

    return _instance;
  }

  Future<SharedPreferences> initPref() async {
    prefs = await SharedPreferences.getInstance();
    return prefs;
  }

  // TOKEN
  String get token {
    return prefs.getString(_TOKEN_KEY);
  }

  String get password {
    print(prefs.getString(_PASSWORD_KEY));
    return prefs.getString(_PASSWORD_KEY);
  }

  set userId(int id) {
    _setId(id);
  }

  _setId(int id) async => await prefs.setInt(_USER_ID, id);

  int get userId {
    return prefs.getInt(_USER_ID);
  }

  set password(String password) {
    _setPassword(password);
  }

  _setPassword(String password) async => await prefs.setString(_PASSWORD_KEY, password);


  set token(String token) {
    _setToken(token);
  }

  _setToken(String token) async => await prefs.setString(_TOKEN_KEY, token);

  String get user {
    return prefs.getString(_USER_KEY);
  }

  set user(String user) {
    _setUser(user);
  }

  _setUser(String user) async => await prefs.setString(_USER_KEY,user);


  // LOCALE
  get locale => prefs.getString(_LOCALE_KEY);

  set locale(String locale) {
    _setLocale(locale);
  }

  _setLocale(String locale) async => await prefs.setString(_LOCALE_KEY, locale);

  // COLD_START
  get isColdStart {
    return prefs.getBool(_COLD_START) ?? true;
  }

  set isColdStart(bool coldStart) {
    _setColdStarPref(coldStart);
  }

  _setColdStarPref(bool coldStart) async =>
      await prefs.setBool(_COLD_START, coldStart);
}
