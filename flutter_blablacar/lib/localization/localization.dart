import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'languages/eng.dart';
import 'languages/ru.dart';
import 'languages/word_keys.dart';

class AppLocalizations {
  AppLocalizations(locale) {
    AppLocalizations.locale = locale;
  }

  static Locale locale;

  static Map<String, Map<WordKeys, String>> localizedValues = {'ru': RU};

  String translate(key) {
    if (localizedValues.keys.contains(locale.languageCode)) {
      return localizedValues[locale.languageCode][key];
    } else {
      return localizedValues["en"][key];
    }
  }

  static String of(BuildContext context, WordKeys key) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations)
        .translate(key);
  }
}

class MyLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  const MyLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(AppLocalizations(locale));
  }

  @override
  bool shouldReload(MyLocalizationsDelegate old) => false;
}
