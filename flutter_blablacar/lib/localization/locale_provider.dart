import 'package:flutter/cupertino.dart';


class LocaleProvider extends ChangeNotifier {
  Locale _locale;

  LocaleProvider(this._locale);

  Locale get value => _locale;

  set value(Locale value) {
    _locale = value;
//    LocalRepository.instance.prefs
//        .setString('language_code', value.languageCode);
//    LocalRepository.instance.prefs.setString('country_code', value.countryCode);
    try{
      notifyListeners();
    }catch(e){
      print('init locale throw exeption');
    }
  }

  set valueWithoutNotify(Locale value) {
    _locale = value;
//    LocalRepository.instance.prefs
//        .setString('language_code', value.languageCode);
//    LocalRepository.instance.prefs.setString('country_code', value.countryCode);
  }
}
