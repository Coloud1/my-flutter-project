
import 'package:flutter_blablacar/localization/languages/word_keys.dart';

const RU = {
  WordKeys.search : 'Найти поездку',
  WordKeys.home : 'Ваши поездки',
  WordKeys.offer: 'Предложить',
  WordKeys.about: 'О себе',
  WordKeys.close: 'Закрыть',
  WordKeys.experience: 'Опыт',
  WordKeys.profile: 'Профиль',
  WordKeys.setting: 'Настройки'
}; // 122 line
