import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_diya/pages/home_page/home_page.dart';
import 'package:flutter_diya/pages/slider_page/slider_widget/slider_widget.dart';

class SliderPage extends StatefulWidget {
  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  PageController _controller;
  int currentIndexPage = 0;
  List<String> _headerText = [
    "Це - Дія",
    "ID - картка",
    "Закордонний паспорт",
    "Цифрове посвідчення водія",
    "Цифровий техпаспорт",
    "Цифровий студентський",
    "Страховий поліс ОСЦПВ"
  ];
  List<String> _descriptionText = [
    "Цифрова держава у вашому смартфоні: цифрові докумвуенти та посвідчення, інформація про майно, пенсію, сімейний стан тощо.\n \nЗараз все, що держава знає про вас, буде тут, під рукою. Згодом у застосунку з'являться всі онлайн-послуги та національні опитування",
    "Понад 4,4 млн громадян можуть користуватися цифровими паспортами у Дії.",
    "Має таку саму юридичну силу, як і паспорт громадянина України",
    "6 млн оцифрованих посвідчень водія з 9.5 млн",
    "5 млн оцифрованих свідотств про реєстрацію транспортного засобу з 9 млн.",
    "Майже 900 тисяч оциффрованих студентських квитків доступні для всіх життєвих ситуацій студентів",
    "Користуйтеся всіма документами водія у своєму смартфоні"
  ];

  @override
  void initState() {
    super.initState();
    _controller = PageController()..addListener(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFc6d9e8),
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20),
              child: _buildImageRow(),
            ),
            Flexible(
              flex: 5,
              child: Stack(
                children: [
                  PageView.builder(
                    key: Key("PAGE VIEW "),
                    physics: BouncingScrollPhysics(),
                    itemBuilder: (context, i) => SliderWidget(
                      index: i,
                      headerText: _headerText[i],
                      descriptionText: _descriptionText[i],
                      key: Key("Slider Widgets + $i"),
                    ),
                    itemCount: _descriptionText.length,
                    controller: _controller,
                    onPageChanged: (i) {
                      print(i);
                      setState(() {
                        currentIndexPage = i;
                      });
                    },
                  ),
                  Align(
                      alignment: Alignment.bottomRight,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 25, bottom: 15),
                        child: DotsIndicator(
                          dotsCount: 7,
                          position: currentIndexPage.toDouble(),
                          decorator: DotsDecorator(
                              size: Size.fromRadius(5),
                              spacing: EdgeInsets.only(right: 5),
                              color: Colors.transparent,
                              activeSize: Size.fromRadius(5),
                              activeColor: Colors.black,
                              shape: CircleBorder(
                                  side: BorderSide(
                                      color: Colors.black, width: 1.5))),
                        ),
                      )),
                ],
              ),
            ),
            Flexible(flex: 1, child: _buildButton())
          ],
        ),
      ),
    );
  }

  Row _buildImageRow() => Row(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: _buildImages(
              assets: "assets/images/Logo_01.png",
            ),
          ),
          _buildImages(assets: "assets/images/gerb.png")
        ],
      );

  Image _buildImages({String assets}) => Image.asset(
        "$assets",
        width: 50,
        height: 50,
        key: Key("Slider_image"),
      );

  Widget _buildButton() {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          color: const Color(0xFFe2ecf5),
        ),
        GestureDetector(
          onTap: () => Navigator.of(context)
              .pushReplacement(MaterialPageRoute(builder: (context) => HomePage())),
          child: Container(
            width: 170,
            height: 60,
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.all(Radius.circular(30)),
            ),
            child: const Center(
                child: const Text(
              "Увійти",
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: "e-Ukraine",
                  fontWeight: FontWeight.w500,
                  fontSize: 15),
            )),
          ),
        ),
      ],
    );
  }
}
