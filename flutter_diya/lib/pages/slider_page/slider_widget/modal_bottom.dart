import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

class ModalBottom extends StatelessWidget {
  final String headerText;
  final String descriptionText;

  const ModalBottom({Key key, this.headerText, this.descriptionText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => _buildModal(context),
        child: Row(
          children: [
            Icon(Icons.mark_as_unread),
            const Text(
              "Чому так?",
              style: TextStyle(
                  fontFamily: "e-Ukraine",
                  fontWeight: FontWeight.w500,
                  fontSize: 16),
            ),
          ],
        ));
  }

  Future _buildModal(context) async {
    return await showCupertinoModalBottomSheet(
        context: context,
        expand: true,
        builder: (context, scrollController) =>
            Material(child: _buildModalContent(context)));
  }

  Widget _buildModalContent(context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 40, bottom: 20),
      child: SingleChildScrollView(
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Чому так?\n', style: TextStyle(fontSize: 20, fontFamily: "e-Ukraine", fontWeight: FontWeight.w500)),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Icon(Icons.close))
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(right: 25),
              child: Text(
                "Новий реєстр водійських посвідчень був створенний у 2013 році. З 9,5 млн власників водійських посвідчень фото відображалися тільки"
                "у 2.5 млн\n\nНашій ІТ-команді вдалося отримати якісні дані із демографічного реєстру та надати можливість 6 млн українцям отримати"
                "електронні документи.\n\nМи будемо й надалі робити все, щоб якість даних та їх безпека в реєстрах зростали кожного дня."
                "Якщо ваші посвідчення не відобразилися, ви отримажете поради, як це виправити",
                style: TextStyle(fontSize: 17, fontFamily: "e-Ukraine"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
