import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

import 'modal_bottom.dart';

class SliderWidget extends StatelessWidget {
  final int index;
  final String descriptionText;
  final String headerText;

  const SliderWidget(
      {Key key, this.index, this.descriptionText, this.headerText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 75),
                  child: _buildHeader(index: this.index),
                ),
                Expanded(child: Padding(
                  padding: const EdgeInsets.only(right: 30),
                  child: _buildDescription(index: this.index),
                )),
                if (index == 3 || index == 4) const ModalBottom()
              ],
            ),
          ),
          if (index != 0)
            Flexible(flex: 2, child: Image.asset("assets/images/1.jpg")),
        ],
      ),
    );
  }

  Text _buildHeader({int index}) => Text(
        "$headerText",
        style: TextStyle(fontSize: 25, fontFamily: "e-Ukraine"),
      );

  Text _buildDescription({int index}) => Text(
        "\n$descriptionText",
        style: TextStyle(fontSize: 17, fontFamily: "e-Ukraine"),
      );
}
