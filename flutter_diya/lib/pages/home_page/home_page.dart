import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageController _controller;
  Animatable<Color> background;

  @override
  void initState() {
    background = TweenSequence<Color>([
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Color(0xFF74b4d7),
          end: Color(0xFFdec7d9),
        ),
      ),
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Color(0xFFdec7d9),
          end: Color(0xFFbbb9ce),
        ),
      ),
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Color(0xFFbbb9ce),
          end: Color(0xFFf2ede7),
        ),
      ),
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Color(0xFFf2ede7),
          end: Color(0xFFc8d8e7),
        ),
      )
    ]);
    _controller = PageController()
      ..addListener(() {
        print(_controller.page);
      });
    super.initState();

  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: AnimatedBuilder(
        animation: _controller,
        builder: (context, builder){
          final color = _controller.hasClients ? _controller.page / 4 : .0;

         return DecoratedBox(
            decoration: BoxDecoration(color: background.evaluate(AlwaysStoppedAnimation(color))),
            child: PageView.builder(
              physics: BouncingScrollPhysics(),
              itemBuilder: (context, i) {
                return Center(
                  child: Container(color: Colors.white,width: 200, height: 200,),
                );
              },
              itemCount: 4,
              controller: _controller,
            ),
          );
        }
      ),
    );
  }
}
